/*
Navicat MySQL Data Transfer

Source Server         : mysql001
Source Server Version : 50521
Source Host           : localhost:3306
Source Database       : person

Target Server Type    : MYSQL
Target Server Version : 50521
File Encoding         : 65001

Date: 2019-11-27 10:37:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dept`
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept` (
  `deptid` int(11) NOT NULL AUTO_INCREMENT,
  `deptname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`deptid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES ('1', '人事部');
INSERT INTO `dept` VALUES ('2', '财务部');
INSERT INTO `dept` VALUES ('3', '研发部');
INSERT INTO `dept` VALUES ('4', '销售部');
INSERT INTO `dept` VALUES ('5', '董事会');

-- ----------------------------
-- Table structure for `employee`
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` date DEFAULT NULL,
  `employeeNum` varchar(100) DEFAULT NULL,
  `loginName` varchar(100) DEFAULT NULL,
  `loginPassword` varchar(100) DEFAULT NULL,
  `userName` varchar(100) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  `deptId` int(11) DEFAULT NULL,
  `idCaid` varchar(50) DEFAULT NULL,
  `telphone` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `salt` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('1', '2019-11-14', 'emp101', 'zhangsan', '79cfeb94595de33b3326c06ab1c7dbda', '张三丰2', '2', '1', '100101010', '1831881818181', '111@qq.com', '北京', '0', 'abcd');
INSERT INTO `employee` VALUES ('2', '2019-11-13', 'emp102', 'lisi', 'f30aa7a662c728b7407c54ae6bfd27d1', '李四', '1', '1', '100101010', '183188181818', '111@qq.com', '上海', '0', 'hello');
INSERT INTO `employee` VALUES ('3', '2019-11-20', 'emp103', 'wangwu', '123', '王五', '3', '3', '100101010', '183188181818', '111@qq.com', '天津', '1', 'test');
INSERT INTO `employee` VALUES ('4', '2019-11-21', 'emp104', 'zhaoliu', '123', '赵柳', '3', '3', '100101010', '183188181818', '111@qq.com', '广州', '0', 'aade');
INSERT INTO `employee` VALUES ('5', '2019-11-20', 'emp105', 'tianqi', '123', '田七', '4', '3', '100101010', '183188181818', '111@qq.com', '深圳', '1', 'sdaa');
INSERT INTO `employee` VALUES ('6', '2019-11-21', 'emp106', 'liuneng', '123', '刘能', '5', '2', '100101010', '183188181818', '111@qq.com', '南京', '0', 'dsae');
INSERT INTO `employee` VALUES ('7', '2019-11-20', '201501010', 'zhangsan1', 'bba731f76483b98747a65e61b81f1e62', '张三', '8', '1', '372902198708084567', '17718181920', 'AAA@163.com', '海淀区软件园孵化器', '0', 'dasd');

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menuid` int(11) NOT NULL AUTO_INCREMENT,
  `menuname` varchar(255) DEFAULT NULL,
  `upmenuid` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `percode` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`menuid`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '个人信息', '0', 'menu', '/', '/', '1', null);
INSERT INTO `menu` VALUES ('2', '薪酬管理', '0', 'menu', '/', '/', '1', null);
INSERT INTO `menu` VALUES ('3', '人事管理', '0', 'menu', '/', '/', '1', null);
INSERT INTO `menu` VALUES ('4', '招聘管理', '0', 'menu', '/', '/', '1', null);
INSERT INTO `menu` VALUES ('5', '培训管理', '0', 'menu', '/', '/', '1', null);
INSERT INTO `menu` VALUES ('6', '系统管理', '0', 'menu', '/', '/', '1', null);
INSERT INTO `menu` VALUES ('7', '个人信息修改', '1', 'menu', 'msg.html', '/', '1', null);
INSERT INTO `menu` VALUES ('8', '密码修改', '1', 'menu', 'password.html', '/', '1', null);
INSERT INTO `menu` VALUES ('9', '薪酬标准管理', '2', 'menu', '/', '/', '1', null);
INSERT INTO `menu` VALUES ('10', '酬标准审批', '2', 'menu', '/', '/', '1', null);
INSERT INTO `menu` VALUES ('11', '人事档案管理', '3', 'menu', '/', '/', null, null);
INSERT INTO `menu` VALUES ('12', '人事档案审批', '3', 'menu', '/', '/', null, null);
INSERT INTO `menu` VALUES ('13', '职位发布管理', '4', 'menu', '/', '/', null, null);
INSERT INTO `menu` VALUES ('14', '简历管理', '4', 'menu', '/', '/', null, null);
INSERT INTO `menu` VALUES ('15', '面试管理', '4', 'menu', '/', '/', null, null);
INSERT INTO `menu` VALUES ('16', '录用管理', '4', 'menu', '/', '/', null, null);
INSERT INTO `menu` VALUES ('17', '培训计划', '5', 'menu', '/', '/', null, null);
INSERT INTO `menu` VALUES ('18', '培训复核', '5', 'menu', '/', '/', null, null);
INSERT INTO `menu` VALUES ('19', '培训考核和反馈', '5', 'menu', '/', '/', null, null);
INSERT INTO `menu` VALUES ('20', '部门管理', '6', 'menu', '/', '/', null, null);
INSERT INTO `menu` VALUES ('21', '用户管理', '6', 'menu', '/resource/demo2/getusers', 'user:selectall', null, null);
INSERT INTO `menu` VALUES ('22', '职位设置', '6', 'menu', '/resource/demo3/getmenus', '/', null, null);
INSERT INTO `menu` VALUES ('23', '菜单管理', '6', 'menu', '/', '/', null, null);
INSERT INTO `menu` VALUES ('24', '用户新增查询', null, 'permission', '/resource/demo2/getalldepts', 'user:getalldepts', null, null);
INSERT INTO `menu` VALUES ('25', '批量删除用户', null, 'permission', '/resource/demo2/deletebyids', 'user:deletebyids', null, null);
INSERT INTO `menu` VALUES ('26', '用户主键查询', null, 'permission', '/resource/demo2/findbyid', 'user:findbyid', null, null);
INSERT INTO `menu` VALUES ('27', '用户修改', null, 'permission', '/resource/demo2/updateEmp', 'user:updateEmp', null, null);
INSERT INTO `menu` VALUES ('28', '用户新增', null, 'permission', '/resource/demo2/insertEmp', 'user:insertEmp', null, null);
INSERT INTO `menu` VALUES ('29', '单行删除用户', null, 'permission', '/resource/demo2/deletebyid', 'user:deletebyid', null, null);
INSERT INTO `menu` VALUES ('30', '部门二级联动', null, 'permission', '/getroles', 'user:getroles', null, null);

-- ----------------------------
-- Table structure for `middle`
-- ----------------------------
DROP TABLE IF EXISTS `middle`;
CREATE TABLE `middle` (
  `middleid` int(11) NOT NULL AUTO_INCREMENT,
  `menuid` int(11) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL,
  PRIMARY KEY (`middleid`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of middle
-- ----------------------------
INSERT INTO `middle` VALUES ('1', '1', '1');
INSERT INTO `middle` VALUES ('2', '3', '1');
INSERT INTO `middle` VALUES ('3', '4', '1');
INSERT INTO `middle` VALUES ('4', '5', '1');
INSERT INTO `middle` VALUES ('5', '11', '1');
INSERT INTO `middle` VALUES ('7', '15', '1');
INSERT INTO `middle` VALUES ('8', '16', '1');
INSERT INTO `middle` VALUES ('9', '17', '1');
INSERT INTO `middle` VALUES ('10', '19', '1');
INSERT INTO `middle` VALUES ('11', '1', '2');
INSERT INTO `middle` VALUES ('12', '7', '2');
INSERT INTO `middle` VALUES ('13', '8', '2');
INSERT INTO `middle` VALUES ('14', '3', '2');
INSERT INTO `middle` VALUES ('15', '11', '2');
INSERT INTO `middle` VALUES ('16', '12', '2');
INSERT INTO `middle` VALUES ('17', '4', '2');
INSERT INTO `middle` VALUES ('18', '16', '2');
INSERT INTO `middle` VALUES ('19', '5', '2');
INSERT INTO `middle` VALUES ('20', '18', '2');
INSERT INTO `middle` VALUES ('21', '1', '3');
INSERT INTO `middle` VALUES ('22', '7', '3');
INSERT INTO `middle` VALUES ('23', '8', '3');
INSERT INTO `middle` VALUES ('24', '4', '3');
INSERT INTO `middle` VALUES ('25', '13', '3');
INSERT INTO `middle` VALUES ('26', '1', '4');
INSERT INTO `middle` VALUES ('27', '7', '4');
INSERT INTO `middle` VALUES ('28', '8', '4');
INSERT INTO `middle` VALUES ('29', '4', '4');
INSERT INTO `middle` VALUES ('30', '13', '4');
INSERT INTO `middle` VALUES ('31', '1', '5');
INSERT INTO `middle` VALUES ('32', '7', '5');
INSERT INTO `middle` VALUES ('33', '8', '5');
INSERT INTO `middle` VALUES ('34', '2', '5');
INSERT INTO `middle` VALUES ('35', '9', '5');
INSERT INTO `middle` VALUES ('36', '1', '6');
INSERT INTO `middle` VALUES ('37', '7', '6');
INSERT INTO `middle` VALUES ('38', '8', '6');
INSERT INTO `middle` VALUES ('39', '2', '6');
INSERT INTO `middle` VALUES ('40', '9', '6');
INSERT INTO `middle` VALUES ('41', '10', '6');
INSERT INTO `middle` VALUES ('42', '1', '8');
INSERT INTO `middle` VALUES ('43', '2', '8');
INSERT INTO `middle` VALUES ('44', '3', '8');
INSERT INTO `middle` VALUES ('45', '4', '8');
INSERT INTO `middle` VALUES ('46', '5', '8');
INSERT INTO `middle` VALUES ('47', '6', '8');
INSERT INTO `middle` VALUES ('48', '7', '8');
INSERT INTO `middle` VALUES ('49', '8', '8');
INSERT INTO `middle` VALUES ('50', '9', '8');
INSERT INTO `middle` VALUES ('51', '10', '8');
INSERT INTO `middle` VALUES ('52', '11', '8');
INSERT INTO `middle` VALUES ('53', '12', '8');
INSERT INTO `middle` VALUES ('54', '13', '8');
INSERT INTO `middle` VALUES ('55', '14', '8');
INSERT INTO `middle` VALUES ('56', '15', '8');
INSERT INTO `middle` VALUES ('57', '16', '8');
INSERT INTO `middle` VALUES ('58', '17', '8');
INSERT INTO `middle` VALUES ('59', '18', '8');
INSERT INTO `middle` VALUES ('60', '19', '8');
INSERT INTO `middle` VALUES ('61', '20', '8');
INSERT INTO `middle` VALUES ('62', '21', '8');
INSERT INTO `middle` VALUES ('63', '22', '8');
INSERT INTO `middle` VALUES ('64', '23', '8');
INSERT INTO `middle` VALUES ('65', '7', '1');
INSERT INTO `middle` VALUES ('66', '8', '1');
INSERT INTO `middle` VALUES ('67', '1', '11');
INSERT INTO `middle` VALUES ('68', '7', '11');
INSERT INTO `middle` VALUES ('69', '8', '11');
INSERT INTO `middle` VALUES ('70', '2', '11');
INSERT INTO `middle` VALUES ('71', '9', '11');
INSERT INTO `middle` VALUES ('72', '10', '11');
INSERT INTO `middle` VALUES ('73', '24', '8');
INSERT INTO `middle` VALUES ('74', '25', '8');
INSERT INTO `middle` VALUES ('75', '26', '8');
INSERT INTO `middle` VALUES ('76', '27', '8');
INSERT INTO `middle` VALUES ('77', '28', '8');
INSERT INTO `middle` VALUES ('78', '29', '8');
INSERT INTO `middle` VALUES ('79', '30', '8');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `rname` varchar(255) DEFAULT NULL,
  `deptid` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `rolenum` varchar(255) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  PRIMARY KEY (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '人事专员', '1', null, null, null, null);
INSERT INTO `role` VALUES ('2', '人事经理', '1', null, null, null, null);
INSERT INTO `role` VALUES ('3', '招聘专员', '1', null, null, null, null);
INSERT INTO `role` VALUES ('4', '招聘经理', '1', null, null, null, null);
INSERT INTO `role` VALUES ('5', '薪酬专员', '2', null, null, null, null);
INSERT INTO `role` VALUES ('6', '薪酬经理', '2', null, null, null, null);
INSERT INTO `role` VALUES ('7', '公司领导', '5', null, null, null, null);
INSERT INTO `role` VALUES ('8', '系统管理员', '5', null, null, null, null);
INSERT INTO `role` VALUES ('10', '人事总监1', '1', 'ddsd', 'dsds', 'QMXC-001002', '1');
INSERT INTO `role` VALUES ('11', '人事总监2', '1', 'qqq', 'qqq', 'QMXC-001002', '1');
INSERT INTO `role` VALUES ('12', '人事总监3', '1', 'eee', 'eee', 'QMXC-001002', '1');
