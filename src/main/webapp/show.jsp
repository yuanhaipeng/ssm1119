<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/11/19
  Time: 9:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
   <h1>show.jsp</h1>
<table border="1" cellspacing="0" width="800px">
    <tr>
        <td>编号</td>
        <td>id</td>
        <td>姓名</td>
        <td>密码</td>
        <td>生日</td>
    </tr>
   <c:forEach items="${stulist}" var="stu" varStatus="sta">
       <tr>
           <td>${sta.count}</td>
           <td>${stu.sid}</td>
           <td>${stu.sname2}</td>
           <td>${stu.password}</td>
           <td><fmt:formatDate value="${stu.birthday}" pattern="yyyy-MM-dd"></fmt:formatDate></td>
       </tr>
   </c:forEach>
</table>
</body>
</html>
