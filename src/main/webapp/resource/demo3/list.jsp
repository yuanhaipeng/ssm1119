<%@page language="java" pageEncoding="utf-8" contentType="text/html; charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

        <title>薪酬标准管理</title>

        <link href="../../css/mine.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <style>
            .tr_color{background-color: #9F88FF}
        </style>
        <div class="div_head">
            <span>
                <span style="float: left;">当前位置是：系统设置-》职称管理</span>
                <span style="float: right; margin-right: 8px; font-weight: bold;">
                    <a style="text-decoration: none;" href="/resource/demo3/getdepts">【添加】</a>
					<a style="text-decoration: none;" href="#">【删除】</a>
                </span>
            </span>
        </div>
        <div></div>
        <div class="div_search">
            <span>
                <form action="/resource/demo3/getmenus" method="get">
                    职称名称: 
					<input type="text"  value="${title}" name="title" />
					 所属部门 
					<input type="text"  value="${deptname}" name="deptname" />
					 状态
					<select>
							<option>是</option>
							<option>否</option>
						</select>
                    <input value="查询" type="submit" />
					
                </form>
            </span>
        </div>
        <div style="font-size: 13px; margin: 10px 5px;">
            <table class="table_a" border="1" width="100%">
                <tr style="font-weight: bold;">
                    <td width="40px;">序号</td>
                    <td width="30px;"><input type="checkbox" /></td>
                    <td width="80px;">职称编码</td>
                    <td >职称名称</td>
                    <td width="100px;">所属部门</td>
                    <td width="120px;">是否启用</td>
                    <td align="center" width="100px;">操作</td>
                </tr>
                <tbody>

                <c:forEach items="${pi.list}" var="role" varStatus="sta">
                <tr style="font-weight: bold;">
                        <td width="40px;">${sta.count}</td>
                        <td width="30px;"><input type="checkbox" /></td>						
                        <td width="80px;">${role.roleid}</td>
                        <td >${role.rname}</td>
						<td width="100px;">${role.dept.deptname}</td>
                        <td width="120px;">是否启用</td>                       
                        <td align="center" width="100px;">删除
                            <a href="/resource/demo3/findbyroleid?rid=${role.roleid}">更新</a></td>
                    </tr>
                </c:forEach>

                <tr>
                    <td colspan="20" style="text-align: center;">
                        <a style="text-decoration: none;" href="/resource/demo3/getmenus?deptname=${deptname}&title=${title}">首页</a>
                        <a style="text-decoration: none;" href="/resource/demo3/getmenus?pageindex=${pi.prePage==0?1:pi.prePage}&deptname=${deptname}&title=${title}">上一页</a>
                        <c:forEach var="i" begin="1" end="${pi.pages}">
                            <a style="text-decoration: none;" href="/resource/demo3/getmenus?pageindex=${i}&deptname=${deptname}&title=${title}">${i}</a>
                        </c:forEach>
                        <a style="text-decoration: none;" href="/resource/demo3/getmenus?pageindex=${pi.nextPage==0?pi.pages:pi.nextPage}&deptname=${deptname}&title=${title}">下一页</a>
                        <a style="text-decoration: none;" href="/resource/demo3/getmenus?pageindex=${pi.pages}&deptname=${deptname}&title=${title}">尾页</a>
                        共${pi.total}条  ${pi.pageNum}/${pi.pages}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>