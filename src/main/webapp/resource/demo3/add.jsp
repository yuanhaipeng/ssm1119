<%@page language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <title>职位管理</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8">
        <link href="../../css/mine.css" type="text/css" rel="stylesheet">
    </head>

    <body>

        <div class="div_head">
            <span>
                <span style="float:left">当前位置是：-》职位管理</span>
                <span style="float:right;margin-right: 8px;font-weight: bold">
                    <a style="text-decoration: none" href="list.html">【返回】</a>
                </span>
            </span>
        </div>
        <div></div>

        <div style="font-size: 13px;margin: 10px 5px">
            <form action="/resource/demo3/addrole" method="post">
            <table border="1" width="100%" class="table_a">
                <tr>
                    <td width="120px;">职位编码<span style="color:red">*</span>：</td>
                    <td><input type="text" name="rolenum" value="QMXC-001002" /></td>
                </tr>
                <tr>
                    <td>职位名称<span style="color:red">*</span>：</td>
                    <td>
                       <input type="text" name="rname" value="人事总监" />
                    </td>
                </tr>

                <tr>
                    <td>职位分类<span style="color:red">*</span>：</td>
                    <td>
						<select>
							<option>管理</option>
							<option>技术</option>
							<option>实施</option>
						</select>
					</td>
                </tr>
                <tr>
                    <td>所属部门<span style="color:red">*</span>：</td>
                    <td>
                       
						 <select name="deptid">
                             <c:forEach items="${depts}" var="dept">
							        <option value="${dept.deptid}">${dept.deptname}</option>
                             </c:forEach>
						</select>
                    </td>
                </tr>
				
				<tr>
                    <td>菜单权限<span style="color:red">*</span>：</td>
                    <td>
                        <c:forEach items="${menus}" var="m1">
                        <input type="checkbox" value="${m1.menuId}" name="mid"/>${m1.menuName}<br/>
                            <c:forEach items="${m1.secondMenu}" var="m2">
					        &nbsp;&nbsp;&nbsp;<input  value="${m2.menuId}" name="mid" type="checkbox"/>${m2.menuName}<br/>
                            </c:forEach>
                        </c:forEach>
                    </td>
                </tr>


				<tr>
                    <td>职位描述<span style="color:red">*</span>：</td>
                    <td>
                        <textarea name="content"></textarea>
                    </td>                
                </tr>

				<tr>
                    <td>备       注：</td>
                    <td>
                        <textarea name="desc"></textarea>
                    </td>                
                </tr>
				<tr>
                    <td>是否启用：</td>
                    <td>
                        <select name="state">
							<option value="1">是</option>
							<option value="0">否</option>
						</select>
                    </td>                
                </tr>
				
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="添加">
						<input type="submit" value="清空">
                    </td>
                </tr>  
            </table>
            </form>
        </div>
    </body>
</html>