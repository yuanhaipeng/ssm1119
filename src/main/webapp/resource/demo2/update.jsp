<%@page pageEncoding="utf-8" language="java" contentType="text/html; charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <title>用户管理</title>
        <meta http-equiv="content-type" content="text/html;charset=utf-8">
        <link href="../../css/mine.css" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="/js/jquery-1.8.0.min.js"></script>
        <script type="text/javascript">
            $(function(){
                $("#dept").change(function () {
                    //1.获得被选中的value值
                   var did= $(this).val();
                   if(did==-1){
                       alert("请选择部门");
                   }else{
                       //将did的值传给服务器，服务器进行查询
                       $.ajax({
                           url:"/getroles",
                           data:"did="+did,
                           type:"post",
                           dataType:"json",
                           success:function(rs){//rs用来接受返回的结果
                                 $("#role")[0].length=0;//让下拉框的长度为0
                                for(var i=0;i<rs.length;i++){
                                    $("#role")[0].add(new Option(rs[i].rname,rs[i].roleid));//Option(显示到标签对中的值,value值)
                                }
                           }
                       })
                   }
                });
            })
        </script>
    </head>

    <body>

        <div class="div_head">
            <span>
                <span style="float:left">当前位置是：-》资源管理》用户管理</span>
                <span style="float:right;margin-right: 8px;font-weight: bold">
                    <a style="text-decoration: none" href="list.html">【返回】</a>
                </span>
            </span>
        </div>
        <div></div>

        <div style="font-size: 13px;margin: 10px 5px">
            <form action="/resource/demo2/updateEmp" method="post">
                <input type="hidden" name="id" value="${emp.id}">
                <table border="1" width="100%" class="table_a">
                <tr>
                    <td width="120px;">用户编号<span style="color:red">*</span>：</td>
                    <td><input type="text" readonly name="employeenum" value="${emp.employeenum}" /></td>

                </tr>
                <tr>
                    <td>登录账号<span style="color:red">*</span>：</td>
                    <td>
                       <input type="text" name="loginname" value="${emp.loginname}" />
                    </td>
                </tr>
               
                <tr>
                    <td>用户姓名<span style="color:red">*</span>：</td>
                    <td>
						<input type="text" name="username" value="${emp.username}" /></td>
                </tr>
                <tr>
                    <td>密码<span style="color:red">*</span>：</td>
                    <td>
						<input type="text"  name="loginpassword" value="${emp.loginpassword}" />
					</td>
                </tr>
                <tr>
                    <td>所属部门<span style="color:red">*</span>：</td>
                    <td>

                        <select name="deptid" id="dept">
                            <c:forEach items="${depts}" var="dept" >
                               <option value="${dept.deptid}" ${dept.deptid==emp.deptid?"selected":""}>${dept.deptname}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>所属职位<span style="color:red">*</span>：</td>
                    <td>
                        
						<select id="role" name="roleid">
							  <c:forEach items="${roles}" var="r">
                                  <option value="${r.roleid}" ${r.roleid==emp.roleid?"selected":""}>${r.rname}</option>
                              </c:forEach>
						</select>
                    </td>
                </tr>




				<tr>
                    <td>身份证<span style="color:red">*</span>：</td>
                    <td>
                        <input type="text" name="idcaid" value="${emp.idcaid}" />
                    </td>                
                </tr>

				<tr>
                    <td>联系电话<span style="color:red">*</span>：</td>
                    <td>
                        <input type="text" name="telphone" value="${emp.telphone}"/>
                    </td>                
                </tr>

				<tr>
                    <td>邮箱<span style="color:red">*</span>：</td>
                    <td>
                        <input type="text" name="email" value="${emp.email}" />
                    </td>                
                </tr>

				<tr>
                    <td>联系地址：</td>
                    <td>
                        <input type="text" name="address" value="${emp.address}" />
                    </td>                
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="修改">
						<input type="reset" value="清空">
                    </td>
                </tr>  
            </table>
            </form>
        </div>
    </body>
</html>