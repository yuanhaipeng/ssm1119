<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

        <title>用户管理</title>

        <link href="../../css/mine.css" type="text/css" rel="stylesheet" />
        <script type="text/javascript">
            function  tosub() {
                //设置隐藏域的值
                document.getElementById("mymethod").value="delete";
                //提交表单
              //  document.getElementById("myform").submit();
                document.forms[1].submit();
            }

            function todaochu(){
                //设置隐藏域的值
                document.getElementById("mymethod").value="daochu";
                //提交表单
                //  document.getElementById("myform").submit();
                document.forms[1].submit();
            }
        </script>
    </head>
    <body>
        <style>
            .tr_color{background-color: #9F88FF}
        </style>
        <div class="div_head">
            <span>
                <span style="float: left;">当前位置是：系统管理-》用户管理</span>
                <span style="float: right; margin-right: 8px; font-weight: bold;">
                    <span onclick="todaochu()" style="color: blue; cursor: pointer">【导出数据】</span>
                    <a style="text-decoration: none;" href="/resource/demo2/getalldepts">【添加】</a>
					<span onclick="tosub()" style="color: blue; cursor: pointer">【删除】</span>
                </span>
            </span>
        </div>
        <div></div>
        <div class="div_search">
            <span>
                <form action="/resource/demo2/getusers" method="post">
                     姓名：
					<input type="text" name="username" value="${uname}"  />
					 角色: 
					<input type="text" name="rolename"  value="${rname}" />
					 所属部门: 
					<input type="text" name="deptname"  value="${dname}" />
                    <input value="查询" type="submit" />
                </form>
            </span>
        </div>
        <div style="font-size: 13px; margin: 10px 5px;">
            <form  id="myform" action="/resource/demo2/deletebyids" method="post">
                <input type="hidden" name="method" id="mymethod">
            <table class="table_a" border="1" width="100%">
                <tbody>
						<tr style="font-weight: bold;">						
                        <td width="40px;">序号</td>
                        <td width="30px;"><input type="checkbox" /></td>						
						<td width="80px;">账号</td>
                        <td width="80px;">姓名</td>
                        <td >角色</td>
						<td width="100px;">所属部门</td>                                        
                        <td align="center" width="200px;">操作</td>
                    </tr>
                <c:forEach items="${pi.list}" varStatus="sta" var="emp">
                    <tr id="product1">
                        <td>${sta.count}</td>
                        <td><input type="checkbox"  name="empid" value="${emp.id}"/></td>
						<td>${emp.loginname}</td>
                        <td><a href="view.html">${emp.username}</a></td>
						<td>${emp.role.rname}</td>
						<td>${emp.dept.deptname}</td>
                        <td>
							<a href="/resource/demo2/findbyid?uid=${emp.id}">修改</a>
                            <c:if test="${emp.id!=sessionScope.user.id}">
							<a href="/resource/demo2/deletebyid?uid=${emp.id}">删除</a>
                            </c:if>
						</td>                        
                    </tr>
                </c:forEach>
                    <tr>
                        <td colspan="20" style="text-align: center;">						
                            <a style="text-decoration: none;" href="/resource/demo2/getusers?username=${uname}&rolename=${rname}&deptname=${dname}">首页</a>
                            <a style="text-decoration: none;" href="/resource/demo2/getusers?index=${pi.prePage==0?1:pi.prePage}&username=${uname}&rolename=${rname}&deptname=${dname}">上一页</a>
                            <c:forEach var="i" begin="1" end="${pi.pages}">
                                <a style="text-decoration: none;" href="/resource/demo2/getusers?index=${i}&username=${uname}&rolename=${rname}&deptname=${dname}">${i}</a>
                            </c:forEach>
                            <a style="text-decoration: none;" href="/resource/demo2/getusers?index=${pi.nextPage==0?pi.pages:pi.nextPage}&username=${uname}&rolename=${rname}&deptname=${dname}">下一页</a>
                            <a style="text-decoration: none;" href="/resource/demo2/getusers?index=${pi.pages}&username=${uname}&rolename=${rname}&deptname=${dname}">尾页</a>
                            共${pi.total}条  ${pi.pageNum}/${pi.pages}
                        </td>
                    </tr>
                </tbody>
            </table>
            </form>
        </div>
    </body>
</html>