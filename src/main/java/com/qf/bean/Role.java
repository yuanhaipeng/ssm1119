package com.qf.bean;

import java.util.List;

//一方
public class Role {
    private Integer roleid;

    private String rname;

    private Integer deptid;

    private String content;//描述
    private String desc;//备注
    private String rolenum;//职位编号
    private List<Employee> employeeList;
    private List<Menu> menus;//存放菜单权限的集合

    public List<Menu> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<Menu> permissionList) {
        this.permissionList = permissionList;
    }

    private List<Menu> permissionList;//存放功能权限的集合
    private Dept dept;
    private List<Integer> menuids;

    public List<Integer> getMenuids() {
        return menuids;
    }

    public void setMenuids(List<Integer> menuids) {
        this.menuids = menuids;
    }

    private Integer state;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getRolenum() {
        return rolenum;
    }

    public void setRolenum(String rolenum) {
        this.rolenum = rolenum;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }



    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname == null ? null : rname.trim();
    }

    public Integer getDeptid() {
        return deptid;
    }

    public void setDeptid(Integer deptid) {
        this.deptid = deptid;
    }
}