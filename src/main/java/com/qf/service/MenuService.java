package com.qf.service;

import com.github.pagehelper.PageInfo;
import com.qf.bean.Menu;

import java.util.List;

/**
 * 2019/11/23
 * Administrator
 * ssm1119
 * 面向对象面向君  不负代码不负卿
 */
public interface MenuService {
    //根据角色id查询从菜单信息
    public List<Menu> findMenusByRoleid(int roleid);
    //查询全部的菜单信息
    public List<Menu> findallMenus();
}
