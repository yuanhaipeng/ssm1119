package com.qf.service;

import com.github.pagehelper.PageInfo;
import com.qf.bean.Menu;
import com.qf.bean.Role;

import java.util.List;

public interface RoleService {
    //查询角色列表，参数列表（分页数据+模糊查数据）  ->2+n个参数
    public PageInfo<Role> findmenus(int pageIndex, int pagesize, String title, String deptname, int status);
    //根据部门id查询角色信息
    public List<Role> findbyDeptid(int did);

    int deleteByPrimaryKey(Integer roleid);

    int insert(Role record,int[] mid);
    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer roleid);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);
}