package com.qf.service.impl;

import com.qf.bean.Dept;
import com.qf.dao.DeptMapper;
import com.qf.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 2019/11/20
 * Administrator
 * ssm1119
 * 面向对象面向君  不负代码不负卿
 */
@Service
public class DeptServiceImpl implements DeptService {
    @Autowired
    private DeptMapper deptMapper;

    @Override
    public List<Dept> findalldepts() {
        return deptMapper.findalldepts();
    }

    @Override
    public int deleteByPrimaryKey(Integer deptid) {
        return 0;
    }

    @Override
    public int insert(Dept record) {
        return 0;
    }

    @Override
    public int insertSelective(Dept record) {
        return 0;
    }

    @Override
    public Dept selectByPrimaryKey(Integer deptid) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(Dept record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(Dept record) {
        return 0;
    }
}
