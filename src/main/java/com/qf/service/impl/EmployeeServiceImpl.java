package com.qf.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.bean.Employee;
import com.qf.bean.Menu;
import com.qf.bean.Role;
import com.qf.dao.EmployeeMapper;
import com.qf.dao.MenuMapper;
import com.qf.dao.RoleMapper;
import com.qf.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 2019/11/19
 * Administrator
 * ssm1119
 * 面向对象面向君  不负代码不负卿
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public PageInfo<Employee> getall(int pageIndex,int pageSize,String username,String rolename,String deptname) {
        //1.指定分页参数
        PageHelper.startPage(pageIndex, pageSize);
        //2.调取dao层方法
        HashMap map = new HashMap();
        map.put("uname",username);
        map.put("rname",rolename);
        map.put("dname",deptname);
        List<Employee> employeeList = employeeMapper.findall(map);
        //3.封装pageInfo
        PageInfo info = new PageInfo(employeeList);
        return info;
    }
    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public Employee login(String username, String password) {
        Employee employee = employeeMapper.login(username);
        if (employee != null) {
            //用户名正确
            if(password.equals(employee.getLoginpassword())){
                    //1.查询当前用户所对应的角色信息
                Role role = roleMapper.selectByPrimaryKey(employee.getRoleid());
                //2.根据角色信息查询角色所对应的菜单列表
                List<Menu> menus = menuMapper.findMenusByRoleid(role.getRoleid());//未分级
                //2.1 对菜单列表分级
               ArrayList firstMenu= new ArrayList();//保存一级菜单
                for (Menu menu : menus) {
                    if(menu.getUpmenuId()==0){ //一级菜单
                        ArrayList secondList= new ArrayList();//保存二级菜单
                        for (Menu second : menus) {
                             if(menu.getMenuId()==second.getUpmenuId()){//如果一级菜单的id(menuid)==二级菜单的父id(upmenuid)
                                 secondList.add(second);
                             }
                        }
                        //将二级菜单的集合封装到一级菜单中
                        menu.setSecondMenu(secondList);
                        //将一级菜单在添加到集合中
                        firstMenu.add(menu);
                    }
                }

                //3.将菜单赋值给角色，角色再赋值给用户
                role.setMenus(firstMenu);
                //4.保存该角色所对应的请求列表（permission）
                List<Menu> permissions = menuMapper.findPermissionByRoleId(role.getRoleid());
                role.setPermissionList(permissions);
                //角色赋给用户
                employee.setRole(role);

                return employee;
            }
            return null;

        }
        return null;
    }

    @Override
    public int deletes(int[] empids) {
        return employeeMapper.deletes(empids);
    }

    @Override
    public List<Employee> findbyids(int[] empid) {
        return employeeMapper.findbyids(empid);
    }

    @Override
    public int deleteByPrimaryKey(Integer id,int state) {
        HashMap map = new HashMap();
        map.put("uid",id);
        map.put("state",state);
        return employeeMapper.deleteByPrimaryKey(map);
    }

    @Override
    @Transactional
    public int insert(Employee record) {
        return employeeMapper.insert(record);
    }

    @Override
    public int insertSelective(Employee record) {
        return 0;
    }

    @Override
    public Employee selectByPrimaryKey(Integer id) {
        return employeeMapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional
    public int updateByPrimaryKeySelective(Employee record) {
        return employeeMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    @Transactional
    public int updateByPrimaryKey(Employee record) {
        return employeeMapper.updateByPrimaryKey(record);
    }
}
