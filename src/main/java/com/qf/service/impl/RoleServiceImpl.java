package com.qf.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.bean.Menu;
import com.qf.bean.Role;
import com.qf.dao.DeptMapper;
import com.qf.dao.MenuMapper;
import com.qf.dao.RoleMapper;
import com.qf.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 2019/11/20
 * Administrator
 * ssm1119
 * 面向对象面向君  不负代码不负卿
 */
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public PageInfo<Role> findmenus(int pageIndex, int pagesize, String title, String deptname, int status) {
        //1.指定分页参数
        PageHelper.startPage(pageIndex,pagesize);
        //2.封装模糊查询的条件
        Map map=new HashMap();
        map.put("title",title);
        map.put("dname",deptname);
        map.put("status",status);
        //3.查询数据
        List<Role> menuList = roleMapper.findall(map);
        //4.返回pageInfo
        return new PageInfo(menuList);
    }

    @Override
    public List<Role> findbyDeptid(int did) {
        return roleMapper.findbyDeptid(did);
    }

    @Override
    public int deleteByPrimaryKey(Integer roleid) {
        return 0;
    }

    @Override
    @Transactional
    public int insert(Role record,int[] mid) {
        //先新增角色信息
        int insert = roleMapper.insert(record);
        //新增中间表需要角色id和菜单id
        System.out.println(5/0);
        //角色id
        Integer roleid = record.getRoleid();
        Map map=new HashMap();
        map.put("roleid",roleid);
        map.put("menuids",mid);
        int i = roleMapper.insertMiddle(map);
        return i;
    }

    @Override
    public int insertSelective(Role record) {
        return 0;
    }


    @Autowired
    private MenuMapper menuMapper;
    @Override
    public Role selectByPrimaryKey(Integer roleid) {
        //1.查询出角色信息
        Role role = roleMapper.selectByPrimaryKey(roleid);
        //2.查询角色所对应的菜单id
        List<Integer> list = menuMapper.findmenuids(roleid);
        role.setMenuids(list);
        return role;
    }

    @Override
    public int updateByPrimaryKeySelective(Role record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(Role record) {
        return 0;
    }
}
