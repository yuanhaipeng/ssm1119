package com.qf.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.bean.Menu;
import com.qf.dao.MenuMapper;
import com.qf.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 2019/11/23
 * Administrator
 * ssm1119
 * 面向对象面向君  不负代码不负卿
 */
@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Menu> findMenusByRoleid(int roleid) {
        return null;
    }

    @Override
    public List<Menu> findallMenus() {
        List<Menu> menus = menuMapper.findallMenus();
        //分级
        ArrayList firstMenu= new ArrayList();//保存一级菜单
        for (Menu menu : menus) {
            if(menu.getUpmenuId()==0){ //一级菜单
                ArrayList secondList= new ArrayList();//保存二级菜单
                for (Menu second : menus) {
                    if(menu.getMenuId()==second.getUpmenuId()){//如果一级菜单的id(menuid)==二级菜单的父id(upmenuid)
                        secondList.add(second);
                    }
                }
                //将二级菜单的集合封装到一级菜单中
                menu.setSecondMenu(secondList);
                //将一级菜单在添加到集合中
                firstMenu.add(menu);
            }
        }

        return  firstMenu;
    }
}
