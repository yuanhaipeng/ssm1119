package com.qf.service;

import com.github.pagehelper.PageInfo;
import com.qf.bean.Employee;

import java.util.List;

public interface EmployeeService{
    //导出查询
    public List<Employee>  findbyids(int[] empid);
    //批量删除
    public int deletes(int[] empids);
    //查询全部
    public PageInfo<Employee> getall(int pageIndex,int pageSize,String username,String rolename,String deptname);
    //登录方法
    public Employee login(String username,String password);

    int deleteByPrimaryKey(Integer id,int state);

    int insert(Employee record);

    int insertSelective(Employee record);

    Employee selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Employee record);

    int updateByPrimaryKey(Employee record);
}