package com.qf.dao;

import com.qf.bean.Dept;

import java.util.List;

public interface DeptMapper {

    //查询全部
    public List<Dept> findalldepts();

    int deleteByPrimaryKey(Integer deptid);

    int insert(Dept record);

    int insertSelective(Dept record);

    Dept selectByPrimaryKey(Integer deptid);

    int updateByPrimaryKeySelective(Dept record);

    int updateByPrimaryKey(Dept record);
}