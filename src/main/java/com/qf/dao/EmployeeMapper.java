package com.qf.dao;

import com.qf.bean.Employee;

import java.util.List;
import java.util.Map;

public interface EmployeeMapper {
    //导出查询
    public List<Employee>  findbyids(int[] empid);
    //批量删除
    public int deletes(int[] empids);
    //查询列表  dao层返回的是list,service返回的是PageInfo
    public List<Employee> findall(Map map);
    //登录
    public Employee login(String name);

    int deleteByPrimaryKey(Map map);

    int insert(Employee record);

    int insertSelective(Employee record);

    Employee selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Employee record);

    int updateByPrimaryKey(Employee record);
}