package com.qf.dao;

import com.qf.bean.Menu;
import com.qf.bean.Role;

import java.util.List;
import java.util.Map;

public interface RoleMapper {
    //新增中间表
    public int insertMiddle(Map map);
    //查询所有角色  dao层的方法的返回值必须是List
    public List<Role> findall(Map map);
    //根据部门id查询角色信息
    public List<Role> findbyDeptid(int did);

    int deleteByPrimaryKey(Integer roleid);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer roleid);

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);
}