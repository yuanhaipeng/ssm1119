package com.qf.dao;

import com.qf.bean.Menu;

import java.util.List;
import java.util.Map;

/**
 * 2019/11/23
 * Administrator
 * ssm1119
 * 面向对象面向君  不负代码不负卿
 */
public interface MenuMapper {
    //根据角色id查询中间表中菜单id
    public List<Integer> findmenuids(int roleid);
    //根据角色id查询从菜单信息
    public List<Menu> findMenusByRoleid(int roleid);
    //查询某个角色的permission列表
    public List<Menu> findPermissionByRoleId(int roleid);

    public List<Menu> findallMenus();

}
