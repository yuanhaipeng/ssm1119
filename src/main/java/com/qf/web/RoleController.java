package com.qf.web;

import com.github.pagehelper.PageInfo;
import com.qf.bean.Dept;
import com.qf.bean.Menu;
import com.qf.bean.Role;
import com.qf.service.DeptService;
import com.qf.service.MenuService;
import com.qf.service.RoleService;
import com.qf.util.filter.enum1.PageEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 2019/11/20
 * Administrator
 * ssm1119
 * 面向对象面向君  不负代码不负卿
 */
@Controller
public class RoleController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private DeptService deptService;

    @Autowired
    private MenuService menuService;
    //更新角色
    @RequestMapping("/resource/demo3/findbyroleid")
    public String findbyid(int rid,ModelMap map){
        //查询角色信息
        Role role = roleService.selectByPrimaryKey(rid);
        map.addAttribute("role",role);
        //查询所有的部门信息
        List<Dept> depts = deptService.findalldepts();
        map.addAttribute("depts",depts);
        //3.查询所有的菜单列表
        List<Menu> menus = menuService.findallMenus();
        map.addAttribute("menus",menus);

        return "/resource/demo3/update";
    }


    //新增角色 mid保存的是选中的权限列表的id值
    @RequestMapping("/resource/demo3/addrole")
    public String addrole(Role role,int[] mid){
        int insert = roleService.insert(role,mid);
        return "redirect:/resource/demo3/getmenus";
    }


    //查询所有的部门
    @RequestMapping("/resource/demo3/getdepts")
    public String getdepts(ModelMap map){
        List<Dept> deptList = deptService.findalldepts();
        map.put("depts",deptList);
        //查询所有的菜单列表
        List<Menu> menus = menuService.findallMenus();
        map.put("menus",menus);
        return "/resource/demo3/add";
    }

    //根据部门id查询角色列表
    @RequestMapping("/getroles")
    @ResponseBody
    public List<Role> getbydeptid(int did){
        List<Role> roles = roleService.findbyDeptid(did);
        return roles;
    }

    //全查+分页:参数都有->pageindex(设置默认值),模糊查询的值,Modelmap
    @RequestMapping("/resource/demo3/getmenus")
    public String getmenus(@RequestParam(defaultValue = "1") int pageindex, String title,
                           String deptname,@RequestParam(defaultValue = "-1") int status,
                           ModelMap map){
        //1.调取serevice方法，返回值必须是PageInfo,2.使用modelMap存模糊查询的条件值(做数据回显)
        //1.调取service方法
        PageInfo<Role> pageInfo = roleService.findmenus(pageindex, PageEnum.PAGESIZE.getSize(), title, deptname, status);
        map.addAttribute("pi",pageInfo);
        map.addAttribute("deptname",deptname);
        map.addAttribute("title",title);


        return "resource/demo3/list";
    }
}
