package com.qf.web;

import cn.com.webxml.MobileCodeWSSoap;
import com.qf.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 2019/11/29
 * Administrator
 * ssm1119
 * 面向对象面向君  不负代码不负卿
 */
@Controller
public class PhoneController {

    @Autowired
    private MobileCodeWSSoap soap;

    @RequestMapping("/getaddress")
    @ResponseBody
    public Message ss1(String phone){
        Message message = new Message();
        String info = soap.getMobileCodeInfo(phone, "");
        if(info.equals("没有此号码记录")){
            message.setAddress(info);
        }else{
            String[] strings = info.split("：");
            message.setAddress(strings[1]);
        }

        return message;
    }
}
