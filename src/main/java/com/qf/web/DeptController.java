package com.qf.web;

import com.qf.bean.Dept;
import com.qf.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 2019/11/20
 * Administrator
 * ssm1119
 * 面向对象面向君  不负代码不负卿
 */
@Controller
public class DeptController {

    @Autowired
    private DeptService service;

    @RequestMapping("/resource/demo2/getalldepts")
    public String getall(ModelMap map){
        List<Dept> deptList = service.findalldepts();
        map.addAttribute("dlist",deptList);
        return "resource/demo2/add";
    }
}
