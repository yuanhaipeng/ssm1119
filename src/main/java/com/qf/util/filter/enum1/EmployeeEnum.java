package com.qf.util.filter.enum1;

import javax.ws.rs.DELETE;

public enum EmployeeEnum {
    //用户状态
    ONLINE(1,"未删除"),//未删除
    DELETE(0,"已删除");//已删除

    private int isline;
    private String state;

    public int getIsline() {
        return isline;
    }



    public String getState() {
        return state;
    }


    EmployeeEnum(int isline, String state){
        this.isline=isline;
        this.state=state;
    }
}
