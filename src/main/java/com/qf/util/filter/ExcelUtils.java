package com.qf.util.filter;

import com.qf.bean.Employee;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * 2019/11/22
 * Administrator
 * ssm1119
 * 面向对象面向君  不负代码不负卿
 */
//定义导出数据的代码
public class ExcelUtils {

    //1.先创建一个Excel文件
    static  HSSFWorkbook workbook;
    //2.创建文件中的sheet表   参数是sheet名称
    static HSSFSheet sheet;
    //3.创建sheet中的行列信息
    //3.1 创建第一行
    public  static void createFirst(String headname){
        workbook=new HSSFWorkbook();
        sheet= workbook.createSheet(headname);
        HSSFRow row = sheet.createRow(0);//创建第一行
        String[] heads={"账号","姓名","所属部门","角色"};
        for(int i=0;i<heads.length;i++){
            HSSFCell cell = row.createCell(i);//创建列
            cell.setCellValue(heads[i]);
        }
    }
    //3.2 创建除第一行以外的其他行 参数表示的是被导出的数据集合
    public static void createOthers(List<Employee> list){
          for(int i=0;i<list.size();i++){
              HSSFRow row = sheet.createRow(i+1);
              Employee employee = list.get(i);
              HSSFCell cella = row.createCell(0);//第一列
              cella.setCellValue(employee.getEmployeenum());
              HSSFCell cellb = row.createCell(1);//第2列
              cellb.setCellValue(employee.getUsername());
              HSSFCell cellc = row.createCell(2);//第3列
              cellc.setCellValue(employee.getDept().getDeptname());
              HSSFCell celld = row.createCell(3);//第4列
              celld.setCellValue(employee.getRole().getRname());
          }
    }
    //4.设置导出信息
    public static void export(OutputStream outputStream) throws IOException {
        //设置输出的方式是以网格的方式输出
        sheet.setGridsPrinted(true);
        //将文件写入到输出流中
        workbook.write(outputStream);
    }


}
