package com.qf.util;

/**
 * 2019/11/29
 * Administrator
 * ssm1119
 * 面向对象面向君  不负代码不负卿
 */
public class Message {
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
